karbo(bagel, 140).
karbo(biscuit, 86).
karbo(white_bread, 96).
karbo(cornflakes, 130).
karbo(macaroni, 238).
karbo(naan_bread, 300).
karbo(noodles, 175).
karbo(pasta, 330).
karbo(porridge_oats, 193).
karbo(boiled_potatoes, 210).
karbo(roasted_potatoes, 420).
karbo(white_rice, 420).
karbo(fried_rice, 500).
karbo(brown_rice, 405).
karbo(spaghetti, 303).

protein(roast_beef, 300).
protein(beef_burger, 320).
protein(chicken, 220).
protein(roast_duck, 400).
protein(crab, 200).
protein(roast_lamb, 300).
protein(boiled_lobster, 200).
protein(mackarel, 320).
protein(prawn, 180).
protein(salmon, 220).
protein(sardines, 220).
protein(sausage_roll, 290).
protein(tuna, 180).
protein(turkey, 200).

serat(apple, 44).
serat(banana, 107).
serat(broccoli, 27).
serat(cabbage, 15).
serat(carrot, 16).
serat(caulliflower, 20).
serat(celery, 5).
serat(cherry, 35).
serat(dates, 100).
serat(grapes, 32).
serat(kiwi, 40).
serat(lettuce, 4).
serat(melon, 14).
serat(orange, 40).
serat(pear, 45).
serat(pineapple, 40).
serat(spinach, 8).
serat(sweetcorn, 95).
serat(tomato, 30).

go :-
    write('Input Kalori: '), read(Input), getmenu(Input, X), 
    write('Menu yang disarankan adalah: '), write(X), nl, !.

menu(Kalori, X):-
    karbo(Karbohidrat, KaloriK),
    protein(Protein, KaloriP),
    serat(Serat, KaloriS),
    Kalori>=KaloriK+KaloriP+KaloriS,
    Kalori-10=<KaloriK+KaloriP+KaloriS,
    X = [Karbohidrat, Protein, Serat].

getmenu(Kalori, X) :-
    bagof(Comb,menu(Kalori,Comb), List),
    random_permutation(List,Y),
    member(X, Y).